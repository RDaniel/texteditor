package hr.fer.zemris.ooup.lab3.notepad;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardStack;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManager;

public interface Plugin {
	String getName();

	String getDescription();

	void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack);
}