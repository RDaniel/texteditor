package hr.fer.zemris.ooup.lab3.notepad.util;

public class LocationRange {

	private Location start = new Location(0, 0);

	private Location end = new Location(0, 0);

	public LocationRange() {
	}

	public LocationRange(LocationRange locationRange) {
		set(locationRange);
	}

	public LocationRange(Location start, Location end) {
		setStart(start);
		setEnd(end);
	}

	public Location getStart() {
		return start;
	}

	public void setStart(Location start) {
		this.start.set(start);
	}

	public void set(LocationRange locationRange) {
		setStart(locationRange.getStart());
		setEnd(locationRange.getEnd());
	}

	public Location getEnd() {
		return end;
	}

	public void setEnd(Location end) {
		this.end.set(end);
	}

	public boolean selectionExists() {
		return start.compareTo(end) != 0;
	}

	public void setUpRangePosition(int x, int y) {
		start.setX(x);
		start.setY(y);
		end.setX(x);
		end.setY(y);
	}

	@Override
	public String toString() {
		return "Start : " + start + " End : " + end;
	}
}