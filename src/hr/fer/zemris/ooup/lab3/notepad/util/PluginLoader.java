package hr.fer.zemris.ooup.lab3.notepad.util;

import hr.fer.zemris.ooup.lab3.notepad.Plugin;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditor;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class PluginLoader {

	private static final String PLUGIN_LOCATION = "out/production/3LAB/hr/fer/zemris/ooup/lab3/notepad/plugins";
	public static final String PLUGIN_PACKAGE = "hr.fer.zemris.ooup.lab3.notepad.plugins.";

	public static List<Plugin> loadPlugins() {
		List<Plugin> plugins = new ArrayList<>();
		File file = new File(PLUGIN_LOCATION);
		File[] files = file.listFiles();
		for (File pluginFile : files) {
			Class<Plugin> clazz;
			try {
				clazz = (Class<Plugin>) Class.forName(PLUGIN_PACKAGE + pluginFile.getName().replace(".class", ""));
				Constructor<?> ctr = clazz.getConstructor();

				plugins.add((Plugin) ctr.newInstance());
			} catch (Exception ignored) {
				System.out.println();
			}
		}

		return plugins;
	}
}