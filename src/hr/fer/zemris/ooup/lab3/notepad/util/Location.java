package hr.fer.zemris.ooup.lab3.notepad.util;

public class Location implements Comparable<Location> {

	public static final Location START = new Location(0, 0);

	private int x;

	private int y;

	public Location(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public Location(Location location) {
		set(location);
	}

	public void incrementX() {
		x++;
	}

	public void decrementX() {
		x--;
	}

	public void incrementY() {
		y++;
	}

	public void decrementY() {
		y--;
	}

	public void set(Location location) {
		x = location.getX();
		y = location.getY();
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public int compareTo(Location o) {
		if (x == o.getX() && y == o.getY()) {
			return 0;
		} else if (y > o.getY() || (y == o.getY() && x > o.getX())) {
			return 1;
		} else {
			return -1;
		}
	}

	@Override
	public String toString() {
		return "Row : " + x + " Column : " + y;
	}
}
