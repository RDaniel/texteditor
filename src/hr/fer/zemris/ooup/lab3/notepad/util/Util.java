package hr.fer.zemris.ooup.lab3.notepad.util;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;

public class Util {

	public static void clearSelection(TextEditorModel editorModel, LocationRange selectionRange) {
		if (selectionRange.selectionExists()) {
			selectionRange.setUpRangePosition(0, 0);
			editorModel.setSelectionRange(selectionRange);
		}
	}

}