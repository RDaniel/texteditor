package hr.fer.zemris.ooup.lab3.notepad.demo;

import javax.swing.*;
import java.awt.*;

public class SwingDemo extends JFrame {

	public SwingDemo() {
		setSize(500, 450);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("Window Demo!");
		setVisible(true);

		initGUI();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(SwingDemo::new);
	}

	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		DemoComponent component = new DemoComponent(this);
		cp.add(component, BorderLayout.CENTER);
	}
}
