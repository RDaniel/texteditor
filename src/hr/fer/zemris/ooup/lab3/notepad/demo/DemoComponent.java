package hr.fer.zemris.ooup.lab3.notepad.demo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class DemoComponent extends JComponent {

	public DemoComponent(JFrame frame) {
		setFocusable(true);

		addKeyListener(new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					frame.dispose();
				}
			}

		});
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		int height = getHeight();
		int width = getWidth();

		g2d.setColor(Color.RED);
		g2d.drawLine(0, height / 2, width, height / 2);
		g2d.drawLine(width / 2, 0, width / 2, height);

		setFont(new Font("Dialog", Font.BOLD, 25));
		FontMetrics fontMetrics = g.getFontMetrics();

		final String first = "This is first row! We are so happy!";
		final String second = "This is second row! We are so sad!";


		g2d.setColor(Color.MAGENTA);
		g2d.drawString(first, 0, fontMetrics.getHeight() + 5);
		g2d.drawString(second, 0, fontMetrics.getHeight() * 2 + 5 * 2);
	}

}