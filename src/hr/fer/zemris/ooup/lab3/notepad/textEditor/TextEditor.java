package hr.fer.zemris.ooup.lab3.notepad.textEditor;

import hr.fer.zemris.ooup.lab3.notepad.util.Location;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;
import hr.fer.zemris.ooup.lab3.notepad.util.Util;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class TextEditor extends JComponent implements CursorObserver, TextObserver {

	private static final int SPACE_BETWEEN_LINES = 2;

	private TextEditorModel editorModel;

	private Location cursorLocation = Location.START;

	private LocationRange selectionRange = new LocationRange();

	private ClipboardStack clipboardStack = new ClipboardStack();

	private boolean shiftPressed;

	public TextEditor(TextEditorModel editorModel) {
		this.editorModel = editorModel;

		editorModel.setSelectionRange(selectionRange);
		editorModel.addCursorObserver(this);
		editorModel.addTextObserver(this);

		addKeyListeners();
		addMouseListener();

		setFocusable(true);
	}

	public TextEditorModel getEditorModel() {
		return editorModel;
	}

	public LocationRange getSelectionRange() {
		return selectionRange;
	}

	public ClipboardStack getClipboardStack() {
		return clipboardStack;
	}

	@Override
	protected void paintComponent(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;

		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, getWidth(), getHeight());
		g2d.setColor(Color.BLACK);

		drawSelectionRange(g2d);
		drawText(g2d);
		drawCursor(g2d);
	}

	private void drawText(Graphics2D g2d) {
		FontMetrics metrics = g2d.getFontMetrics();
		int i = 1;
		for (String line : editorModel) {
			g2d.drawString(line, 0, metrics.getHeight() * i + SPACE_BETWEEN_LINES * i);
			i++;
		}
	}

	private void drawSelectionRange(Graphics2D g2d) {
		selectionRange = editorModel.getSelectionRange();
		if (!selectionRange.selectionExists()) {
			return;
		}

		FontMetrics metrics = g2d.getFontMetrics();

		Location start = selectionRange.getStart();
		Location end = selectionRange.getEnd();
		if (start.compareTo(end) > 0) {
			Location temp = start;
			start = end;
			end = temp;
		}

		Color currentColor = g2d.getColor();
		g2d.setColor(Color.CYAN);

		int counter = 0;
		while (start.getY() + counter <= end.getY()) {
			int y = start.getY() + counter;
			int startX = metrics.stringWidth(editorModel.getTextLines().get(start.getY()).substring(0, start.getX()));
			int startY = y * metrics.getHeight() + (y + 1) * SPACE_BETWEEN_LINES + 4;

			int height = metrics.getHeight();
			int offset = y == start.getY() ? startX : 0;

			if (y != end.getY()) {
				int width = metrics.stringWidth(editorModel.getTextLines().get(y)) - offset;
				g2d.fillRect(offset, startY, width, height);
			} else {
				int width = metrics.stringWidth(editorModel.getTextLines().get(y).substring(0, end.getX())) - offset;
				g2d.fillRect(offset, startY, width, height);
			}

			counter++;
		}

		g2d.setColor(currentColor);
	}

	private void drawCursor(Graphics2D g2d) {
		FontMetrics metrics = g2d.getFontMetrics();

		int y = cursorLocation.getY();
		int x = cursorLocation.getX();
		int cursorX = x == 0 ? 0 : metrics.stringWidth(editorModel.getTextLines().get(y).substring(0, x));
		int cursorY = y * metrics.getHeight() + (y + 1) * SPACE_BETWEEN_LINES + 4;

		g2d.drawLine(cursorX, cursorY, cursorX, cursorY + metrics.getHeight());
	}

	@Override
	public void updateCursorLocation(Location loc) {
		cursorLocation = loc;
		repaint();
	}

	@Override
	public void updateText() {
		repaint();
	}

	private void writeChar(char c) {
		if (getFont().canDisplay(c)) {
			if (selectionRange.selectionExists()) {
				editorModel.deleteRange(selectionRange);
				checkSelection();
			}

			editorModel.insert(c);
		}
	}

	private void checkSelection() {
		if (shiftPressed) {
			selectionRange.setEnd(cursorLocation);
			editorModel.setSelectionRange(selectionRange);
		} else {
			Util.clearSelection(editorModel, selectionRange);
		}
	}

	private void addKeyListeners() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				switch (e.getKeyCode()) {
					case KeyEvent.VK_UP:
						editorModel.moveCursorUp();
						checkSelection();
						break;
					case KeyEvent.VK_DOWN:
						editorModel.moveCursorDown();
						checkSelection();
						break;
					case KeyEvent.VK_RIGHT:
						editorModel.moveCursorRight();
						checkSelection();
						break;
					case KeyEvent.VK_LEFT:
						editorModel.moveCursorLeft();
						checkSelection();
						break;
					case KeyEvent.VK_BACK_SPACE:
						if (selectionRange.selectionExists()) {
							editorModel.deleteRange(selectionRange);
							checkSelection();
						} else {
							editorModel.deleteBefore();
						}
						break;
					case KeyEvent.VK_DELETE:
						if (selectionRange.selectionExists()) {
							editorModel.deleteRange(selectionRange);
							checkSelection();
						} else {
							editorModel.deleteAfter();
						}
						break;
					case KeyEvent.VK_SHIFT:
						shiftPressed = true;
						selectionRange.setUpRangePosition(cursorLocation.getX(), cursorLocation.getY());
						break;
					default:
						writeChar(e.getKeyChar());
						break;
				}
			}

			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_SHIFT) {
					shiftPressed = false;
				}
			}
		});
	}

	private void addMouseListener() {
		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				requestFocus();
			}
		});
	}
}