package hr.fer.zemris.ooup.lab3.notepad.textEditor;

public interface EditAction {

	void executeDo();

	void executeUndo();

}
