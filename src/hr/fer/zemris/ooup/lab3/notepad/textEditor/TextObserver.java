package hr.fer.zemris.ooup.lab3.notepad.textEditor;

public interface TextObserver {

	void updateText();

}
