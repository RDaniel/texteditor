package hr.fer.zemris.ooup.lab3.notepad.textEditor;

import hr.fer.zemris.ooup.lab3.notepad.util.Location;

public interface CursorObserver {

	void updateCursorLocation(Location loc);

}
