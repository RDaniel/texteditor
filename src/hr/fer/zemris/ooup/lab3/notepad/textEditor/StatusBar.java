package hr.fer.zemris.ooup.lab3.notepad.textEditor;

import hr.fer.zemris.ooup.lab3.notepad.util.Location;

import javax.swing.*;
import java.awt.*;

public class StatusBar extends JPanel implements CursorObserver {

	private static final String CURSOR_LOCATION = "Cursor location : ";
	private static final String NUM_OF_LINES = "Number of lines : ";

	private TextEditorModel textEditorModel;

	private JLabel cursorLocationLabel;

	private JLabel numberOfLinesLabel;

	public StatusBar(TextEditorModel textEditorModel) {
		this.textEditorModel = textEditorModel;
		textEditorModel.addCursorObserver(this);

		cursorLocationLabel = new JLabel(CURSOR_LOCATION + textEditorModel.getCursorLocation());
		cursorLocationLabel.setHorizontalAlignment(JLabel.CENTER);

		numberOfLinesLabel = new JLabel(NUM_OF_LINES + textEditorModel.getTextLines().size());
		numberOfLinesLabel.setHorizontalAlignment(JLabel.CENTER);

		setLayout(new GridLayout(1, 2));

		add(cursorLocationLabel);
		add(numberOfLinesLabel);

		setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 2));
	}


	@Override
	public void updateCursorLocation(Location loc) {
		cursorLocationLabel.setText(CURSOR_LOCATION + loc);
		numberOfLinesLabel.setText(NUM_OF_LINES + textEditorModel.getTextLines().size());
		repaint();
	}
}
