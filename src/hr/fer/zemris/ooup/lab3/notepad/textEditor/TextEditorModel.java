package hr.fer.zemris.ooup.lab3.notepad.textEditor;

import hr.fer.zemris.ooup.lab3.notepad.actions.AddedStringEditAction;
import hr.fer.zemris.ooup.lab3.notepad.actions.DeleteRangeEditAction;
import hr.fer.zemris.ooup.lab3.notepad.util.Location;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;

import java.util.*;

public class TextEditorModel implements Iterable<String> {

	private static final char LINE_SEPARATOR = '\n';

	private List<String> textLines = new LinkedList<>();

	private LocationRange selectionRange = new LocationRange();

	private Location cursorLocation = new Location(0, 0);

	private List<CursorObserver> cursorObservers;

	private List<TextObserver> textObservers;

	public TextEditorModel() {
		this("");
	}

	public TextEditorModel(String text) {
		cursorObservers = new ArrayList<>();
		textObservers = new ArrayList<>();

		insert(text, false);
	}

	public Location getCursorLocation() {
		return cursorLocation;
	}

	public void setCursorLocation(Location cursorLocation) {
		this.cursorLocation.set(cursorLocation);
		fireCursorChange();
	}

	public List<String> getTextLines() {
		return textLines;
	}

	public void setTextLines(List<String> textLines) {
		this.textLines = textLines;

		fireTextChange();
	}

	public void moveCursorLeft() {
		if (cursorLocation.getX() == 0) {
			if (cursorLocation.getY() == 0) {
				return;
			}

			cursorLocation.decrementY();
			cursorLocation.setX(textLines.get(cursorLocation.getY()).length());
		} else {
			cursorLocation.decrementX();
		}

		fireCursorChange();
	}

	public void moveCursorRight() {
		if (cursorLocation.getX() == textLines.get(cursorLocation.getY()).length()) {
			if (cursorLocation.getY() == textLines.size() - 1) {
				return;
			}

			cursorLocation.incrementY();
			cursorLocation.setX(0);
		} else {
			cursorLocation.incrementX();
		}

		fireCursorChange();
	}

	public void moveCursorUp() {
		if (cursorLocation.getY() == 0) {
			return;
		}

		cursorLocation.decrementY();

		if (cursorLocation.getX() > textLines.get(cursorLocation.getY()).length()) {
			cursorLocation.setX(textLines.get(cursorLocation.getY()).length());
		}

		fireCursorChange();
	}

	public void moveCursorDown() {
		if (cursorLocation.getY() == textLines.size() - 1) {
			return;
		}

		cursorLocation.incrementY();

		if (cursorLocation.getX() > textLines.get(cursorLocation.getY()).length()) {
			cursorLocation.setX(textLines.get(cursorLocation.getY()).length());
		}

		fireCursorChange();
	}

	public void deleteBefore() {
		if (cursorLocation.compareTo(Location.START) == 0) {
			return;
		}

		moveCursorLeft();
		deleteAfter();

		fireCursorChange();
	}

	public void deleteAfter() {
		String line = textLines.get(cursorLocation.getY());

		Location startPosition = new Location(cursorLocation);
		Location endPosition = new Location(cursorLocation);
		if (endPosition.getX() == line.length()) {
			endPosition.setX(0);
			endPosition.incrementY();
		} else {
			endPosition.incrementX();
		}

		LocationRange range = new LocationRange(startPosition, endPosition);
		UndoManager.getInstance().push(new DeleteRangeEditAction(this, range, getText(range)));

		deleteAtCursor();

		fireTextChange();
	}

	public void deleteRange(LocationRange range) {
		deleteRange(range, true);
	}

	public void deleteRange(LocationRange range, boolean undoAction) {
		if (undoAction) {
			UndoManager.getInstance().push(new DeleteRangeEditAction(this, range, getText(range)));
		}

		Location startLocation = new Location(range.getStart());
		cursorLocation.set(range.getEnd());

		if (startLocation.compareTo(cursorLocation) > 0) {
			Location temp = startLocation;
			startLocation = cursorLocation;
			cursorLocation = temp;
		}

		while (startLocation.compareTo(cursorLocation) != 0) {
			moveCursorLeft();
			deleteAtCursor();
		}

		fireTextChange();
		fireCursorChange();
	}

	public String getSelectedText() {
		return getText(selectionRange);
	}

	public String getText(LocationRange range) {
		StringBuilder builder = new StringBuilder();

		Location start = range.getStart();
		Location end = range.getEnd();
		if (start.compareTo(end) > 0) {
			Location temp = start;
			start = end;
			end = temp;
		}

		int counter = 0;
		while (start.getY() + counter <= end.getY()) {
			int y = start.getY() + counter;

			int offset = y == start.getY() ? start.getX() : 0;
			if (y != end.getY()) {
				builder.append(textLines.get(y).substring(offset));
				builder.append(LINE_SEPARATOR);
			} else {
				builder.append(textLines.get(y), offset, end.getX());
			}

			counter++;
		}

		return builder.toString();
	}

	public LocationRange getSelectionRange() {
		return selectionRange;
	}

	public void setSelectionRange(LocationRange selectionRange) {
		this.selectionRange.set(selectionRange);

		fireCursorChange();
	}

	public void insert(char c) {
		Location startingPosition = new Location(cursorLocation);

		addCharacter(c);

		UndoManager.getInstance().push(new AddedStringEditAction(this, c + "", new LocationRange(startingPosition, cursorLocation)));

		fireTextChange();
	}

	public void insert(String text) {
		insert(text, true);
	}

	public void insert(String text, boolean undoAction) {
		Location startingPosition = new Location(cursorLocation);
		text.chars().forEach(c -> addCharacter((char) c));

		if (undoAction) {
			UndoManager.getInstance().push(new AddedStringEditAction(this, text, new LocationRange(startingPosition, cursorLocation)));
		}

		fireTextChange();
	}

	public void clear() {
		textLines.clear();
		cursorLocation.set(Location.START);

		fireCursorChange();
		fireTextChange();
	}

	public void addRow(String row) {
		textLines.add(row);
	}

	public void fireCursorChange() {
		cursorObservers.forEach(o -> o.updateCursorLocation(cursorLocation));
	}

	public void fireTextChange() {
		textObservers.forEach(TextObserver::updateText);
	}

	public void addCursorObserver(CursorObserver observer) {
		cursorObservers.add(observer);
	}

	public void removeCursorObserver(CursorObserver observer) {
		cursorObservers.remove(observer);
	}

	public void addTextObserver(TextObserver observer) {
		textObservers.add(observer);
	}

	public void removeTextObserver(TextObserver observer) {
		textObservers.remove(observer);
	}

	Iterator<String> allLines() {
		return new LinesIterator(textLines.subList(0, textLines.size()));
	}

	Iterator<String> linesRange(int index1, int index2) {
		if (index1 < 0 || index2 >= textLines.size()) {
			throw new IllegalArgumentException("Invalid index value!");
		}

		return new LinesIterator(textLines.subList(index1, index2));
	}

	@Override
	public Iterator<String> iterator() {
		return allLines();
	}

	private void rearrangeLines() {
		if (cursorLocation.getY() == textLines.size() - 1 ||
				(cursorLocation.getX() == 0) && textLines.get(cursorLocation.getY()).length() != 0) {
			return;
		}

		List<String> tempList = new LinkedList<>();

		for (int i = 0, j = textLines.size(); i < j; i++) {
			if (i == cursorLocation.getY()) {
				tempList.add(textLines.get(i) + textLines.get(i + 1));
			} else if (i != cursorLocation.getY() + 1) {
				tempList.add(textLines.get(i));
			}
		}

		textLines = tempList;
	}

	private void addCharacter(char c) {
		if (textLines.isEmpty()) {
			textLines.add("");
		}

		String line = textLines.get(cursorLocation.getY());

		if (c == LINE_SEPARATOR) {
			String firstPart = line.substring(0, cursorLocation.getX());
			String secondPart = line.substring(cursorLocation.getX());

			textLines.remove(cursorLocation.getY());
			textLines.add(cursorLocation.getY(), firstPart);
			textLines.add(cursorLocation.getY() + 1, secondPart);

			cursorLocation.setX(0);
			cursorLocation.incrementY();
		} else {
			line = line.substring(0, cursorLocation.getX()) + c + line.substring(cursorLocation.getX());

			textLines.remove(cursorLocation.getY());
			textLines.add(cursorLocation.getY(), line);

			moveCursorRight();
		}
	}

	private void deleteAtCursor() {
		String line = textLines.get(cursorLocation.getY());
		if (cursorLocation.getX() == line.length()) {
			rearrangeLines();
		} else {
			deleteCharacter(line);
		}
	}

	private void deleteCharacter(String line) {
		line = line.substring(0, cursorLocation.getX()) + line.substring(cursorLocation.getX() + 1);

		textLines.remove(cursorLocation.getY());
		textLines.add(cursorLocation.getY(), line);
	}

	private static class LinesIterator implements Iterator<String> {

		private List<String> lines;

		private int currentLine;

		public LinesIterator(List<String> lines) {
			this.lines = lines;
		}

		@Override
		public boolean hasNext() {
			return currentLine < lines.size();
		}

		@Override
		public String next() {
			if (!hasNext()) {
				throw new NoSuchElementException("No more lines to iterate!");
			}

			return lines.get(currentLine++);
		}
	}
}