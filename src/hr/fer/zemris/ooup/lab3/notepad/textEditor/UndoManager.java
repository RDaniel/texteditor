package hr.fer.zemris.ooup.lab3.notepad.textEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class UndoManager {

	private static UndoManager ourInstance = new UndoManager();
	private Stack<EditAction> undoStack = new Stack<>();
	private Stack<EditAction> redoStack = new Stack<>();
	private List<UndoManagerObserver> undoManagerObservers = new ArrayList<>();

	private UndoManager() {
	}

	public static UndoManager getInstance() {
		return ourInstance;
	}

	public void undo() {
		if (undoStack.isEmpty()) {
			return;
		}

		EditAction editAction = undoStack.pop();
		editAction.executeUndo();
		redoStack.push(editAction);

		fireChange();
	}

	public void redo() {
		if (redoStack.isEmpty()) {
			return;
		}

		EditAction editAction = redoStack.pop();
		editAction.executeDo();
		undoStack.push(editAction);

		fireChange();
	}

	public void push(EditAction c) {
		redoStack.clear();
		undoStack.push(c);

		fireChange();
	}

	public boolean canUndo() {
		return !undoStack.isEmpty();
	}

	public boolean canRedo() {
		return !redoStack.isEmpty();
	}

	public void addUndoManagerObserver(UndoManagerObserver managerObserver) {
		undoManagerObservers.add(managerObserver);
	}

	public void removeUndoManagerObserver(UndoManagerObserver managerObserver) {
		undoManagerObservers.remove(managerObserver);
	}

	private void fireChange() {
		undoManagerObservers.forEach(UndoManagerObserver::changeHappened);
	}
}
