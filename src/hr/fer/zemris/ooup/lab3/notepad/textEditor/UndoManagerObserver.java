package hr.fer.zemris.ooup.lab3.notepad.textEditor;

public interface UndoManagerObserver {

	void changeHappened();

}