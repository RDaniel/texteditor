package hr.fer.zemris.ooup.lab3.notepad.textEditor;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ClipboardStack {

	private Stack<String> texts = new Stack<>();

	private List<ClipboardObserver> clipboardObservers = new ArrayList<>();

	public void addText(String text) {
		texts.push(text);

		fireChange();
	}

	public String popText() {
		if (isEmpty()) {
			return "";
		}

		String text =  texts.pop();
		fireChange();

		return text;
	}

	public String peekText() {
		if (isEmpty()) {
			return "";
		}
		return texts.peek();
	}

	public void clear() {
		texts.clear();
	}

	public boolean isEmpty() {
		return texts.isEmpty();
	}

	private void fireChange() {
		clipboardObservers.forEach(ClipboardObserver::changeHappened);
	}

	public void addClipboardObserver(ClipboardObserver clipboardObserver) {
		clipboardObservers.add(clipboardObserver);
	}

	public void removeClipboardObserver(ClipboardObserver clipboardObserver) {
		clipboardObservers.remove(clipboardObserver);
	}
}