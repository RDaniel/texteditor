package hr.fer.zemris.ooup.lab3.notepad.plugins;

import hr.fer.zemris.ooup.lab3.notepad.Plugin;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardStack;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManager;

import javax.swing.*;
import java.awt.*;

public class StatisticPlugin extends JDialog implements Plugin {

	private static final String LINES = "Number of lines : ";
	private static final String WORDS = "Number of words : ";
	private static final String LETTERS = "Number of letters : ";

	private JLabel linesLabel;
	private JLabel wordsLabel;
	private JLabel lettersLabel;

	public StatisticPlugin() {
		setTitle("Statistic Plugin!");
		setSize(200, 200);
		setLocation(700, 300);

		setResizable(false);
		setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);

		initGUI();
	}

	private void initGUI() {
		linesLabel = new JLabel();
		linesLabel.setHorizontalAlignment(JLabel.CENTER);

		wordsLabel = new JLabel();
		wordsLabel.setHorizontalAlignment(JLabel.CENTER);

		lettersLabel = new JLabel();
		lettersLabel.setHorizontalAlignment(JLabel.CENTER);

		JPanel panel = new JPanel(new GridLayout(3, 1));

		panel.add(linesLabel);
		panel.add(wordsLabel);
		panel.add(lettersLabel);

		getContentPane().add(panel);
	}

	@Override
	public String getName() {
		return "Statistic";
	}

	@Override
	public String getDescription() {
		return "Generates user statistic!";
	}

	@Override
	public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
		int numberOfLines = model.getTextLines().size();
		int numberOfWords = 0;
		int numberOfLetters = 0;

		for (String line : model) {
			numberOfLetters += line.toCharArray().length;
			numberOfWords += line.split("\\s+").length;
		}

		linesLabel.setText(LINES + numberOfLines);
		wordsLabel.setText(WORDS + numberOfWords);
		lettersLabel.setText(LETTERS + numberOfLetters);

		setVisible(true);
	}
}