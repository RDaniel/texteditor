package hr.fer.zemris.ooup.lab3.notepad.plugins;

import hr.fer.zemris.ooup.lab3.notepad.Plugin;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardStack;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManager;

import java.util.LinkedList;
import java.util.List;

public class UpperPlugin implements Plugin {

	@Override
	public String getName() {
		return "Upper";
	}

	@Override
	public String getDescription() {
		return "Converts every first word letter to upper!";
	}

	@Override
	public void execute(TextEditorModel model, UndoManager undoManager, ClipboardStack clipboardStack) {
		List<String> lines = new LinkedList<>();

		StringBuilder builder = new StringBuilder();
		for (String line : model) {
			builder.setLength(0);

			boolean wasSpace = false;
			for (char c : line.toCharArray()) {
				if (c == ' ') {
					wasSpace = true;
				} else if (wasSpace) {
					wasSpace = false;
					c = Character.toUpperCase(c);
				}

				builder.append(c);
			}

			lines.add(builder.toString());
		}

		model.setTextLines(lines);
	}
}