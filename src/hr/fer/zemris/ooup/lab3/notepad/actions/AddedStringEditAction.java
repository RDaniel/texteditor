package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.EditAction;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;

public class AddedStringEditAction implements EditAction {

	private String text;

	private LocationRange locationRange;

	private TextEditorModel model;

	public AddedStringEditAction(TextEditorModel model, String text, LocationRange locationRange) {
		this.model = model;
		this.text = text;
		this.locationRange = locationRange;
	}

	@Override
	public void executeDo() {
		model.insert(text, false);
	}

	@Override
	public void executeUndo() {
		model.deleteRange(locationRange, false);
	}
}
