package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardObserver;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardStack;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;
import hr.fer.zemris.ooup.lab3.notepad.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class PasteAction extends AbstractAction implements ClipboardObserver {

	private TextEditor editor;

	public PasteAction(TextEditor editor) {
		super("Paste");
		this.editor = editor;

		editor.getClipboardStack().addClipboardObserver(this);
		setEnabled(false);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control V"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		TextEditorModel editorModel = editor.getEditorModel();
		LocationRange selectionRange = editor.getSelectionRange();
		ClipboardStack clipboardStack = editor.getClipboardStack();

		if (selectionRange.selectionExists()) {
			editorModel.deleteRange(selectionRange);
			Util.clearSelection(editorModel, selectionRange);
		}

		editorModel.insert(clipboardStack.peekText());
	}

	@Override
	public void changeHappened() {
		setEnabled(!editor.getClipboardStack().isEmpty());
	}
}