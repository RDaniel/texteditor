package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ClearAction extends AbstractAction {

	private TextEditorModel editorModel;

	public ClearAction(TextEditorModel editorModel) {
		super("Clear editor!");

		this.editorModel = editorModel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		editorModel.clear();
	}
}