package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.Location;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class MoveToEndAction extends AbstractAction {

	private TextEditorModel editorModel;

	public MoveToEndAction(TextEditorModel editorModel) {
		super("Cursor to document end");
		this.editorModel = editorModel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		int size = editorModel.getTextLines().size();
		int x = editorModel.getTextLines().get(size - 1).length();

		editorModel.setCursorLocation(new Location(x, size - 1));
	}
}