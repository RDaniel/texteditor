package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardStack;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;
import hr.fer.zemris.ooup.lab3.notepad.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CutAction extends AbstractAction {

	private TextEditor editor;

	public CutAction(TextEditor editor) {
		super("Cut");
		this.editor = editor;

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control X"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		TextEditorModel editorModel = editor.getEditorModel();
		LocationRange selectionRange = editor.getSelectionRange();
		ClipboardStack clipboardStack = editor.getClipboardStack();

		clipboardStack.addText(editorModel.getSelectedText());
		editorModel.deleteRange(selectionRange);
		Util.clearSelection(editorModel, selectionRange);
	}
}