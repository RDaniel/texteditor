package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManager;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManagerObserver;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class UndoAction extends AbstractAction implements UndoManagerObserver {

	public UndoAction() {
		super("Undo");

		setEnabled(false);
		UndoManager.getInstance().addUndoManagerObserver(this);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Z"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UndoManager.getInstance().undo();
	}

	@Override
	public void changeHappened() {
		setEnabled(UndoManager.getInstance().canUndo());
	}
}
