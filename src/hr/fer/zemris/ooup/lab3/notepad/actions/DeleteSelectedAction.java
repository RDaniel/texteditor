package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;
import hr.fer.zemris.ooup.lab3.notepad.util.Util;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class DeleteSelectedAction extends AbstractAction {

	public TextEditor editor;

	public DeleteSelectedAction(TextEditor editor) {
		super("Delete selected");
		this.editor = editor;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		TextEditorModel editorModel = editor.getEditorModel();
		LocationRange selectionRange = editor.getSelectionRange();

		if (selectionRange.selectionExists()) {
			editorModel.deleteRange(selectionRange);
			Util.clearSelection(editorModel, selectionRange);
		}
	}
}
