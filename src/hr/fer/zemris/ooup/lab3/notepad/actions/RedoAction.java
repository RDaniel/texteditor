package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManager;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManagerObserver;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class RedoAction extends AbstractAction implements UndoManagerObserver {

	public RedoAction() {
		super("Redo");

		setEnabled(false);
		UndoManager.getInstance().addUndoManagerObserver(this);

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control Y"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		UndoManager.getInstance().redo();
	}

	@Override
	public void changeHappened() {
		setEnabled(UndoManager.getInstance().canRedo());
	}
}