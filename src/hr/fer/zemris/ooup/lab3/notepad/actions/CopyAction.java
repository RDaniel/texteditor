package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.ClipboardStack;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class CopyAction extends AbstractAction {

	private TextEditor editor;

	public CopyAction(TextEditor editor) {
		super("Copy");

		this.editor = editor;

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control C"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		ClipboardStack clipboardStack = editor.getClipboardStack();
		TextEditorModel editorModel = editor.getEditorModel();

		clipboardStack.addText(editorModel.getSelectedText());
	}
}