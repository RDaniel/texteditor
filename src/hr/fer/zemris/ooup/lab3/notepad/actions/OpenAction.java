package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.JTextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class OpenAction extends AbstractAction {

	private JTextEditor jEditor;

	private TextEditorModel model;

	public OpenAction(JTextEditor jEditor, TextEditorModel model) {
		super("Open");
		this.jEditor = jEditor;
		this.model = model;

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control N"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Open file!");

		if (fc.showOpenDialog(jEditor) != JFileChooser.APPROVE_OPTION) {
			return;
		}

		Path filePath = fc.getSelectedFile().toPath();

		if (!Files.isReadable(filePath)) {
			JOptionPane.showMessageDialog(jEditor, "File " + filePath + " is not readable.", "Error!",
					JOptionPane.ERROR_MESSAGE);

			return;
		}

		byte[] data;
		try {
			data = Files.readAllBytes(filePath);
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(jEditor, "Error happened while opening the :" + filePath + ".", "Error!", JOptionPane.ERROR_MESSAGE);
			return;
		}

		String text = new String(data, StandardCharsets.UTF_8);
		model.insert(text);
	}
}
