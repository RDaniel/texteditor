package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.Location;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class MoveToStartAction extends AbstractAction {

	private TextEditorModel editorModel;

	public MoveToStartAction(TextEditorModel editorModel) {
		super("Cursor to document start");
		this.editorModel = editorModel;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		editorModel.setCursorLocation(Location.START);
	}
}
