package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.textEditor.EditAction;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.util.Location;
import hr.fer.zemris.ooup.lab3.notepad.util.LocationRange;

public class DeleteRangeEditAction implements EditAction {

	private TextEditorModel model;

	private LocationRange locationRange;

	private String deletedString;

	public DeleteRangeEditAction(TextEditorModel model, LocationRange locationRange, String deletedString) {
		this.model = model;
		this.locationRange = new LocationRange(locationRange);
		this.deletedString = deletedString;
	}

	@Override
	public void executeDo() {
		model.deleteRange(locationRange, false);
	}

	@Override
	public void executeUndo() {
		Location start = new Location(locationRange.getStart());
		Location end = new Location(locationRange.getEnd());

		if (start.compareTo(end) > 0) {
			start = end;
		}

		model.setCursorLocation(start);
		model.insert(deletedString, false);
	}
}