package hr.fer.zemris.ooup.lab3.notepad.actions;

import hr.fer.zemris.ooup.lab3.notepad.JTextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;

public class SaveAction extends AbstractAction {

	private JTextEditor jEditor;

	private TextEditorModel model;

	public SaveAction(JTextEditor jEditor, TextEditorModel model) {
		super("Save");
		this.jEditor = jEditor;
		this.model = model;

		putValue(Action.ACCELERATOR_KEY, KeyStroke.getKeyStroke("control S"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Path openedFilePath;

		JFileChooser fc = new JFileChooser();
		fc.setDialogTitle("Save file!");

		if (fc.showSaveDialog(jEditor) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(jEditor, "Didn't save anything!", "Info!", JOptionPane.ERROR_MESSAGE);
			return;
		}

		openedFilePath = fc.getSelectedFile().toPath();

		if (Files.exists(openedFilePath)) {
			int pressed = JOptionPane.showConfirmDialog(jEditor, "File already exist! Override?", "Override?",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);

			if (pressed == JOptionPane.NO_OPTION) {
				return;
			}
		}

		try {
			for (String line : model) {
				line = line + System.lineSeparator();
				Files.write(openedFilePath, line.getBytes(StandardCharsets.UTF_8));
			}
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(jEditor, "Saving failed! Disk state is unknown.", "Error!",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		JOptionPane.showMessageDialog(jEditor, "File saved.", "Info!", JOptionPane.INFORMATION_MESSAGE);
	}
}
