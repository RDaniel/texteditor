package hr.fer.zemris.ooup.lab3.notepad;

import hr.fer.zemris.ooup.lab3.notepad.actions.*;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.StatusBar;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditor;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.TextEditorModel;
import hr.fer.zemris.ooup.lab3.notepad.textEditor.UndoManager;
import hr.fer.zemris.ooup.lab3.notepad.util.PluginLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

public class JTextEditor extends JFrame {

	private TextEditorModel model;
	private TextEditor editor;

	public JTextEditor() {
		setSize(600, 500);
		setLocation(500, 200);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		setTitle("JTextEditor");
		setVisible(true);

		initGUI();
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(JTextEditor::new);
	}

	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());

		model = new TextEditorModel();
		initEditor(model);
		editor = new TextEditor(model);

		JPanel statusBar = new StatusBar(model);
		createMenuBar();
		JToolBar toolBar = createToolbar();

		cp.add(statusBar, BorderLayout.PAGE_END);
		cp.add(toolBar, BorderLayout.PAGE_START);
		cp.add(editor, BorderLayout.CENTER);

		toolBar.setFocusable(false);
		editor.requestFocus();
	}

	private JToolBar createToolbar() {
		JToolBar toolBar = new JToolBar();
		toolBar.setFloatable(false);

		toolBar.add(new JButton(new UndoAction()));
		toolBar.add(new JButton(new RedoAction()));
		toolBar.addSeparator();
		toolBar.add(new JButton(new CopyAction(editor)));
		toolBar.add(new JButton(new CutAction(editor)));
		toolBar.add(new JButton(new PasteAction(editor)));

		return toolBar;
	}

	private void createMenuBar() {
		JMenuBar menuBar = new JMenuBar();

		JMenu fileMenu = new JMenu("File");
		fileMenu.add(new JMenuItem(new OpenAction(this, model)));
		fileMenu.add(new JMenuItem(new SaveAction(this, model)));
		JMenuItem exitItem = new JMenuItem("Exit");
		exitItem.addActionListener(l -> dispose());
		fileMenu.add(exitItem);

		JMenu editMenu = new JMenu("Edit");
		editMenu.add(new JMenuItem(new UndoAction()));
		editMenu.add(new JMenuItem(new RedoAction()));
		editMenu.add(new JMenuItem(new CutAction(editor)));
		editMenu.add(new JMenuItem(new CopyAction(editor)));
		editMenu.add(new JMenuItem(new PasteAction(editor)));
		editMenu.add(new JMenuItem(new PasteAndTakeAction(editor)));
		editMenu.add(new JMenuItem(new DeleteSelectedAction(editor)));
		editMenu.add(new JMenuItem(new ClearAction(model)));

		JMenu moveMenu = new JMenu("Move");
		moveMenu.add(new MoveToStartAction(model));
		moveMenu.add(new MoveToEndAction(model));

		JMenu plugins = new JMenu("Plugins");
		createPlugins(plugins);

		menuBar.add(fileMenu);
		menuBar.add(editMenu);
		menuBar.add(moveMenu);
		menuBar.add(plugins);

		setJMenuBar(menuBar);
	}

	private void createPlugins(JMenu pluginsMenu) {
		List<Plugin> plugins = PluginLoader.loadPlugins();
		plugins.forEach(plugin -> pluginsMenu.add(new JMenuItem(new AbstractAction(plugin.getName()) {
			@Override
			public void actionPerformed(ActionEvent e) {
				plugin.execute(model, UndoManager.getInstance(), editor.getClipboardStack());
			}
		})));
	}

	private void initEditor(TextEditorModel model) {
		model.addRow("Template text!");
	}
}