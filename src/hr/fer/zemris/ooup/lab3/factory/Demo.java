package hr.fer.zemris.ooup.lab3.factory;

import hr.fer.zemris.ooup.lab3.factory.model.Animal;

public class Demo {

	public static void main(String[] args) throws Exception {
		Animal animal = AnimalFactory.newInstance("Parrot", "Slavica");
		animal.animalPrintGreeting();
		animal.animalPrintMenu();

		System.out.println();

		animal = AnimalFactory.newInstance("Tiger", "Devijant");
		animal.animalPrintGreeting();
		animal.animalPrintMenu();
	}

}
