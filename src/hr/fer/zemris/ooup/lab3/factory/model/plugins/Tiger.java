package hr.fer.zemris.ooup.lab3.factory.model.plugins;

import hr.fer.zemris.ooup.lab3.factory.model.Animal;

public class Tiger extends Animal {

	private String animalName;

	public Tiger(String animalName) {
		this.animalName = animalName;
	}

	@Override
	public String name() {
		return animalName;
	}

	@Override
	public String greet() {
		return "Mjau!";
	}

	@Override
	public String menu() {
		return "Mlako mlijeko!";
	}
}