package hr.fer.zemris.ooup.lab3.factory.model.plugins;

import hr.fer.zemris.ooup.lab3.factory.model.Animal;

public class Parrot extends Animal {

	private String animalName;

	public Parrot(String animalName) {
		this.animalName = animalName;
	}

	@Override
	public String name() {
		return animalName;
	}

	@Override
	public String greet() {
		return "Sto mu gromova!";
	}

	@Override
	public String menu() {
		return "Brazilski orasi!";
	}
}